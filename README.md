# 檔案大小計算

查詢某個目錄下所有檔案和資料夾的大小並以此排序

## 安裝

    git clone https://gitlab.com/liaoke-public/dir-size
    cd dir-size
    npm install

## 執行

    # 直接執行
    node index <dir path>
    # 執行exe
    dirsize <dir path>
    # 範例：dirsize D:/

## 打包exe
    npm run pack

## 執行畫面

![res/screenshot1.png](res/screenshot1.png)

![res/screenshot2.png](res/screenshot2.png)

![res/screenshot3.png](res/screenshot3.png)
