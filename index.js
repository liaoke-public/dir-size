const fs = require('fs')
const chalk = require('chalk')
const path = require('path')
const getSize = require('get-folder-size')
const cliProgress = require('cli-progress')


if(process.argv[2]){
  start(process.argv[2])
}else{
  start('./')
}
async function start(folderPath){
  let isTTY = process.stdout.isTTY
  let progressBar = isTTY ? new cliProgress.SingleBar({
    format: formatter
  }, cliProgress.Presets.shades_classic) : null
  let subfiles = fs.readdirSync(folderPath)
  let progress = 0
  if(isTTY)progressBar.start(subfiles.length, 0)
  let files = []
  for(let fname of subfiles){
    if(isTTY){
      progressBar.update(progress, {
        fname: parseFname(fname)
      })
    }
    let p = path.join(folderPath, fname)
    progress += 0.5
    let info = await getInfo(p)
    if(info.isDir){
      files.push(
        await getSizeAsync(p)
          .then((size)=>{
            return {
              isDir: info.isDir,
              fname,
              size
            }
          })
          .catch((err)=>{
            return {
              isDir: info.isDir,
              fname,
              size: -1,
              err: err
            }
          })
      )
    }else{
      files.push({
        isDir: info.isDir,
        fname,
        size: info.size
      })
    }
    progress += 0.5
  }
  let mb = 1024*1024
  if(isTTY){
    progressBar.update(subfiles.length)
    progressBar.stop()
  }
  files = files.filter((item)=>{
    return item.size > 0
  })
  files = files.sort((a, b)=>{
    return a.size - b.size 
  })
  // 這邊決定輸出的樣式
  files = files.map((item)=>{
    let desc, sizeText
    let fname
    if(item.size >= mb){
      desc = `${item.fname} : ${(item.size / mb).toFixed(2)} MB`
      sizeText = `${(item.size / mb).toFixed(2)} ${chalk.yellow('MB')}`
    }else{
      sizeText = `${(item.size / 1024).toFixed(2)} ${chalk.green('KB')}`
    }
    if(big(item.size)){
      sizeText = chalk.red(sizeText)
    }
    if(item.isDir){
      fname = `"${chalk.blueBright(item.fname)}"`
    }else{
      fname = `"${item.fname}"`
    }
    desc = `${sizeText} - ${fname}`
    return desc
  })
  console.log(files.join('\n'))
}
function big(size){
  big.size = big.size || 100 * 1024 * 1024
  return size >= big.size
}
function getSizeAsync(folder){
  return new Promise((resolve, reject)=>{
    getSize(folder, (err, size) => {
      if(err)return reject(err)
      resolve(size)
    })
  })
}
function formatter(opts, params, payload){
  const bar = 
    opts.barCompleteString.substr(0, Math.round(params.progress*opts.barsize)) + 
    opts.barIncompleteString.substr(0, Math.round((1-params.progress)*opts.barsize))
  let { value, total } = params
  let percentage = (100 * value / total).toFixed(0)
  let { fname } = payload
  return `${percentage}% | ${value.toFixed(0)}/${total} |${bar}| ${fname}`
}
function parseFname(fname){
  fname = fname.length > 33 ? fname.substr(0, 30) + '...' : fname
  return fname
}
function getInfo(p){
  return new Promise((resolve, reject)=>{
    fs.stat(p, (err, stats)=>{
      if(err)return resolve({
        err,
        isDir: false,
        size: 0
      })
      resolve({
        size: stats.size,
        isDir: stats.isDirectory()
      })
    })
  })
}